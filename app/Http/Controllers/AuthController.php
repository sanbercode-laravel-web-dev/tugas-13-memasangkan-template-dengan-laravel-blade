<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register() {
        return view('form');
    }
    public function welcome_page(Request $request) {
        $first_name = $request['first_name'];
        $last_name = $request['last_name'];
        $gender = $request['gender'];
        $nationality = $request['nationality'];
        $bio = $request['bio'];
        return view('welcome-page', [
            'first_name' => $first_name,
             'last_name'=> $last_name,
             'gender'=> $gender,
             'nationality'=> $nationality,
             'bio'=> $bio,
            ]);
    }
}
